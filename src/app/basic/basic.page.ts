import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.page.html',
  styleUrls: ['./basic.page.scss'],
})
export class BasicPage implements OnInit {

  nombre: number;

  constructor() { }

  ngOnInit() {
    this.nombre = 0;
  }

  increase() {
    if(this.nombre < 9) this.nombre++;
  }

  decrease() {
    if(this.nombre > 0) this.nombre--;
  }
}
