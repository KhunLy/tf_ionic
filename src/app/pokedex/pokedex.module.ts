import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PokedexPageRoutingModule } from './pokedex-routing.module';

import { PokedexPage } from './pokedex.page';
import { PokeDetailsComponent } from './poke-details/poke-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PokedexPageRoutingModule
  ],
  entryComponents: [
    PokeDetailsComponent
  ],
  declarations: [
    PokedexPage,
    PokeDetailsComponent
  ]
})
export class PokedexPageModule {}
