export interface Result {
    name: string;
    url: string;
}

export interface PokemonsRequest {
    count: number;
    next?: string;
    previous?: string;
    results: Result[];
}