import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Task } from '../_models/task';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.page.html',
  styleUrls: ['./to-do.page.scss'],
})
export class ToDoPage implements OnInit {

  newTask: string;
  tasks: Task[]
  
  constructor(
    private actionSheetCtrl: ActionSheetController,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('TASKS').then(tasks => {
      this.tasks = tasks || [];
    });
    
  }

  add() {
    if(this.newTask) {
      this.tasks.push({ 
        name: this.newTask, 
        isChecked: false  
      });
      this.newTask = null;
      this.save();
    }
  }

  async presentActionSheet(item:Task){
    const actionsheet = await this.actionSheetCtrl.create({
      header: 'Actions',
      buttons: [{
        text: 'Delete',
        icon: 'trash',
        role: 'destructive',
        handler: () => {
          let ind = this.tasks.indexOf(item);
          this.tasks.splice( ind, 1);
          this.save();
          //this.tasks = this.tasks.filter(x => x != item);
        }
      },{
        text: 'Check',
        icon: 'checkmark',
        handler: () => {
          item.isChecked = true;
          this.save();
        }
      }]
    });
    actionsheet.present();
  }

  private save() {
    this.storage.set('TASKS', this.tasks);
  }

}
