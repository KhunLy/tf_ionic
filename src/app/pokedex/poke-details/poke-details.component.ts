import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PokemonsService } from 'src/app/_services/pokemons.service';
import { PokemonDetailsRequest } from 'src/app/_models/pokemon-details-request';

@Component({
  selector: 'app-poke-details',
  templateUrl: './poke-details.component.html',
  styleUrls: ['./poke-details.component.scss'],
})
export class PokeDetailsComponent implements OnInit {

  @Input() url:string;

  model: PokemonDetailsRequest

  constructor(
    private modalCtrl: ModalController,
    private pokeService: PokemonsService
  ) { }

  ngOnInit() {
    this.pokeService.getDetails(this.url)
      .subscribe(data => {
        this.model = data;
      });
  }

  close() { this.modalCtrl.dismiss(); }

}
