import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PokemonsRequest } from '../_models/pokemons-request';
import { PokemonDetailsRequest } from '../_models/pokemon-details-request';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  private readonly endPoint: string
    = 'https://pokeapi.co/api/v2/pokemon/';
  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(url:string = null): Observable<PokemonsRequest> {
    return this.httpClient
      .get<PokemonsRequest>(url || this.endPoint);
  }

  getDetails(url: string): Observable<PokemonDetailsRequest> {
    return this.httpClient
      .get<PokemonDetailsRequest>(url);
  }
}
