import { Component, OnInit } from '@angular/core';
import { PokemonsService } from '../_services/pokemons.service';
import { PokemonsRequest, Result } from '../_models/pokemons-request';
import { ModalController } from '@ionic/angular';
import { PokeDetailsComponent } from './poke-details/poke-details.component';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.page.html',
  styleUrls: ['./pokedex.page.scss'],
})
export class PokedexPage implements OnInit {

  model: PokemonsRequest;
  constructor(
    private pokeS: PokemonsService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    // appeler un service qui va rechercher mes pokemons
    this.pokeS.getAll().subscribe(data => {
      this.model = data;
    }, error => {}, () => {});
  }

  previous() {
    this.pokeS
      .getAll(this.model.previous).subscribe(data => {
      this.model = data;
    }, error => {}, () => {});
  }

  next() {
    this.pokeS
      .getAll(this.model.next).subscribe(data => {
      this.model = data;
    }, error => {}, () => {});
  }

  async presentModal(item: Result) {
    let modal = await this.modalCtrl.create({
      component: PokeDetailsComponent,
      componentProps: {
        url: item.url
      }
    });
    modal.present();
  }

}
